variable "pool_name" {
  type = string
  description = "The name of the Xen-Orchestra pool"
}

variable "vms" {
  type = map(
    object({
      image_name = string
      template_name = optional(string, "Generic Linux UEFI")
      network_name = optional(string, "Pool-wide network associated with eth0")
      storage_method = optional(string, "Local storage")
      storage_amount = number
      cpu_amount = number
      memory_amount = number
      tags = optional(list(string), [])
    })
  )
}
