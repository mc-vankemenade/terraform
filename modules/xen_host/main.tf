data "xenorchestra_pool" "this" {
    name_label = var.pool_name
}

data "xenorchestra_vdi" "this" {
  for_each = var.vms
  name_label = each.value.image_name
  pool_id = data.xenorchestra_pool.this.id
}

data "xenorchestra_sr" "this" {
  for_each = var.vms
  name_label = each.value.storage_method
  pool_id = data.xenorchestra_pool.this.id
}

data "xenorchestra_template" "this" {
    for_each = var.vms
    name_label = each.value.template_name
    pool_id = data.xenorchestra_pool.this.id
}

data "xenorchestra_network" "this" {
  for_each = var.vms
  name_label = each.value.network_name
  pool_id = data.xenorchestra_pool.this.id
}

resource "xenorchestra_vm" "talos-controlplane_1" {
  for_each = var.vms
  memory_max = each.value.memory_amount
  cpus  = each.value.cpu_amount
  name_label = each.key
  template = data.xenorchestra_template.this[each.key].id

  disk {
    sr_id = data.xenorchestra_sr.this[each.key].id
    name_label = "${each.key}-disk"
    size = each.value.storage_amount
  }

  cdrom {
      id = data.xenorchestra_vdi.this[each.key].id
  }

  network {
      network_id = data.xenorchestra_network.this[each.key].id
  }

  tags = each.value.tags
  lifecycle {
    ignore_changes = [destroy_cloud_config_vdi_after_boot, template]
  }
}
