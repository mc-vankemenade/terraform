variable "cluster_name" {
  type = string
}

variable "cluster_endpoint" {
  type = string
}

variable "controlplane_nodes" {
  type = list(string)
}

variable "worker_nodes" {
  type = list(string)
}

variable "enable_controlplane_scheduling" {
  type = bool
  default = false
}
