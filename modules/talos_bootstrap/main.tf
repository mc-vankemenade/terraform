resource "talos_machine_secrets" "this" {
}

data "talos_client_configuration" "this" {
  cluster_name         = var.cluster_name
  client_configuration = talos_machine_secrets.this.client_configuration
  nodes                = toset(concat(var.controlplane_nodes, var.worker_nodes))
}

data "talos_machine_configuration" "controlplane" {
  cluster_name     = var.cluster_name
  machine_type     = "controlplane"
  cluster_endpoint = var.cluster_endpoint
  machine_secrets  = talos_machine_secrets.this.machine_secrets
  config_patches   = var.enable_controlplane_scheduling ? [<<EOF
cluster:
  allowSchedulingOnControlPlanes: true
EOF
] : []
}

data "talos_machine_configuration" "worker" {
  cluster_name     = var.cluster_name
  machine_type     = "worker"
  cluster_endpoint = var.cluster_endpoint
  machine_secrets  = talos_machine_secrets.this.machine_secrets
}

resource "talos_machine_configuration_apply" "controlplane" {
  count = length(var.controlplane_nodes)

  client_configuration        = talos_machine_secrets.this.client_configuration
  machine_configuration_input = data.talos_machine_configuration.controlplane.machine_configuration
  node                        = var.controlplane_nodes[count.index]
  config_patches = [
    yamlencode({
      machine = {
        nodeLabels = var.enable_controlplane_scheduling ? {
          "node.kubernetes.io/exclude-from-external-load-balancers" = {"$patch" = "delete"}
        } : {}
        install = {
          disk = "/dev/xvda"
        }
        network = {
          hostname = "talos-cp-${count.index}.local"
          nameservers = ["192.168.10.1", "8.8.8.8"]
          interfaces = [
            {
              interface = "enX0"
              addresses = ["192.168.10.${110 + count.index}/24"]
              routes = [
                {
                  network = "0.0.0.0/0"
                  gateway = "192.168.10.1"
                }
              ]
            }
          ]
        }
      }
    })
  ]
}

resource "talos_machine_configuration_apply" "worker" {
  count = length(var.worker_nodes)

  client_configuration        = talos_machine_secrets.this.client_configuration
  machine_configuration_input = data.talos_machine_configuration.worker.machine_configuration
  node                        = var.worker_nodes[count.index]
  config_patches = [
    yamlencode({
      machine = {
        install = {
          disk = "/dev/xvda"
        }
        network = {
          hostname = "talos-${count.index}.local"
          nameservers = ["192.168.10.1", "8.8.8.8"]
          interfaces = [
            {
              interface = "enX0"
              addresses = ["192.168.10.${120 + count.index}/24"]
              routes = [
                {
                  network = "0.0.0.0/0"
                  gateway = "192.168.10.1"
                }
              ]
            }
          ]
        }
      }
    })
  ]
}

resource "talos_machine_bootstrap" "controlplane" {
  node                 = var.controlplane_nodes[0]
  client_configuration = talos_machine_secrets.this.client_configuration

  depends_on = [talos_machine_configuration_apply.controlplane, talos_machine_configuration_apply.worker]
}
resource "talos_cluster_kubeconfig" "this" {
  client_configuration = talos_machine_secrets.this.client_configuration
  node                 = var.controlplane_nodes[0]

  depends_on = [talos_machine_bootstrap.controlplane]
}

output "kubeconfig" {

  value = nonsensitive(talos_cluster_kubeconfig.this.kubeconfig_raw)
}
