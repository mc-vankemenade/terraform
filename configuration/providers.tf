terraform {
  required_providers {
    xenorchestra = {
      source = "vatesfr/xenorchestra"
      version = "0.29.0"
    }
    talos = {
      source = "siderolabs/talos"
      version = "0.7.0-alpha.0"
    }
  }
}

provider "xenorchestra" {
  # Must be ws or wss
  url      = "ws://localhost" # Or set XOA_URL environment variable
  username = "admin@admin.net"
  password = "admin"
  insecure = true              # Or set XOA_INSECURE environment variable to any value
}

provider "talos" {}