module "host_1_vms" {
  source = "../modules/xen_host"
  pool_name = "xcp-host-1"
  vms = {
    "control-plane-1" = {
      image_name = "talos-amd64.iso"
      storage_amount = 53687091200 // 50 GiB
      cpu_amount = 3
      memory_amount = 8589934592 // 8 GiB
      tags = [
        "kubernetes",
        "control-plane"
      ]
    },
    "worker-1" = {
      image_name = "talos-amd64.iso"
      storage_amount = 53687091200 // 50 GiB
      cpu_amount = 3
      memory_amount = 8589934592 // 8 GiB
      tags = [
        "kubernetes",
        "worker"
      ]
    },
    "worker-2" = {
      image_name = "talos-amd64.iso"
      storage_amount = 53687091200 // 50 GiB
      cpu_amount = 3
      memory_amount = 8589934592 // 8 GiB
      tags = [
        "kubernetes",
        "worker"
      ]
    }
  }
}
