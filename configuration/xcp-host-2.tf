module "host_2_vms" {
  source = "../modules/xen_host"
  pool_name = "xcp-host-2"
  vms = {
    "worker-3" = {
      image_name = "talos-amd64.iso"
      storage_amount = 53687091200 // 50 GiB
      cpu_amount = 3
      memory_amount = 8589934592 // 8 GiB
      tags = [
        "kubernetes",
        "worker"
      ]
    },
    "worker-4" = {
      image_name = "talos-amd64.iso"
      storage_amount = 53687091200 // 50 GiB
      cpu_amount = 3
      memory_amount = 8589934592 // 8 GiB
      tags = [
        "kubernetes",
        "worker"
      ]
    },
    "worker-5" = {
      image_name = "talos-amd64.iso"
      storage_amount = 53687091200 // 50 GiB
      cpu_amount = 3
      memory_amount = 8589934592 // 8 GiB
      tags = [
        "kubernetes",
        "worker"
      ]
    }
  }
}
