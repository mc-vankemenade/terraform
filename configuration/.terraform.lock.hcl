# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/siderolabs/talos" {
  version     = "0.7.0-alpha.0"
  constraints = "0.7.0-alpha.0"
  hashes = [
    "h1:DhDoTsmNZpRNVlIrwJLJOpDzZnnLm2Qed+X1waDAVxo=",
    "zh:090d86eee971ac84a1d6999d1ccdb1323f257ced6aec068ac39f621d9410baad",
    "zh:0fa82a384b25a58b65523e0ea4768fa1212b1f5cfc0c9379d31162454fedcc9d",
    "zh:3de44dd80dee28b4e5840886167b2a0abab16dd8aefa1d387f913e57723bf74a",
    "zh:3eb60ee11290e32cb436aa6c2801fe16f436388ee3578f913656776590634835",
    "zh:5d31feb8a7782a5f77cfd7e4447f731d9f69c9350a1cf08ec98b66bd014bbb2a",
    "zh:6b0c1d0965fd256ac38911add83a95a179d69843978956c5a2980c073f209b8d",
    "zh:75431c28ac8a09243291e95d9ce93ae250bc77e1e40c81e94b84639dfca3e492",
    "zh:7f26210ddc7af32737756ce214b208218a2c1679475e3eb49504543911e7d9ad",
    "zh:8e5b685a8db6ddb28db84df076729389a3fb8cbe0576f996ab7e0a0a31220b4e",
    "zh:b441337a78d2fbcea9cf0261ddc45599dd332459700e87484f1258d656399f6a",
    "zh:bbb54c313bf26845b012d3d74f4d300cea96dbb2d7410e7210e64cb0be2d1f60",
    "zh:c7991f7c27762ba17404ee6b666e7b66c6cd8bf24f01103c0d2ed96a40021b5e",
    "zh:d8901ba224dc14f4d6cab1509a4d2f7bd87958fa3d7840c70f59fa2967f77515",
    "zh:dc425e059399527f44e493cc8a078244065b4c0f5a77bbd9f00f3b47fb4a27d3",
    "zh:de3b15a809b49ef9fee77f9f864c2ed1bdbfa62fc258c59473169269f354d8f5",
  ]
}

provider "registry.opentofu.org/vatesfr/xenorchestra" {
  version     = "0.29.0"
  constraints = "0.29.0"
  hashes = [
    "h1:KUQOLHqXwLAYHqtyOzvfpwu4BWSEGnGl5F7isi8jr7M=",
    "zh:097b5e2b779d15045551868b3a3407e16caa60c431476d2ac8b9c7fb3806fe78",
    "zh:11e73bf9a98832d604ea9aaf7ed6ccf82c9969bff4c2ced5ab8035d15c1a2ddd",
    "zh:33a0de184df4dce9450d5bbfa3c7a2eb93605de27d1d6927f192d9f7319efb34",
    "zh:6bbd066bc464585b6bc0c26223d2d0dbd1e26ff637df72ed3105b0fb2c62fb19",
    "zh:aabce1d037b505b15e09bb9235bd0379419237e2129094411c9ffeeb42c365cf",
    "zh:da3ce66b13d2226bc230ea1c659872908d568b21a07b63a17b774ab5ae871dee",
    "zh:fc6aa03bd54d59aa057e724ca061871d64293f333d5c58f127b1e0304c8b7ec0",
  ]
}
