module "talos_cluster_bootstrap" {
  source = "../modules/talos_bootstrap"

  cluster_name = "talos_cluster"
  cluster_endpoint = "https://controlplane.talos.local:6443"

  controlplane_nodes = ["192.168.10.110"]
  worker_nodes = ["192.168.10.120", "192.168.10.121", "192.168.10.122", "192.168.10.123", "192.168.10.124"]
}

output "kubeconfig" {
  value = module.talos_cluster_bootstrap.kubeconfig
}
